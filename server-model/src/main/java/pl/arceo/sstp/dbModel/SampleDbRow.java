package pl.arceo.sstp.dbModel;

import javax.persistence.Entity;

@Entity
public class SampleDbRow extends BaseBean{

	private String rowValue;

	public String getRowValue() {
		return rowValue;
	}

	public void setRowValue(String rowValue) {
		this.rowValue = rowValue;
	}
}
