package pl.arceo.sstp.dbModel;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseBean {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Override
	public int hashCode() {
		if (id==null) return 123;
		return id.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (id==null) return this==obj;
		if (obj instanceof BaseBean) {
			BaseBean toComp = (BaseBean) obj;
			if (toComp.id==null) return false;
			return id.equals(toComp.id);
		}
		else return false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
