package pl.arceo.sstp.web.model;

public class IndexFormModel {
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
