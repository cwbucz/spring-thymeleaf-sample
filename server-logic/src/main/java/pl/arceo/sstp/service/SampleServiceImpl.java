package pl.arceo.sstp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.arceo.sstp.dbModel.SampleDbRow;
import pl.arceo.sstp.repository.SampleRepository;

@Service
public class SampleServiceImpl implements SampleService {

	@Autowired private SampleRepository repo;
	
	@Transactional
	@Override
	public List<SampleDbRow> selectSampleRows() {
		return repo.selectSampleRows();
	}

	@Transactional
	@Override
	public void insertOrUpdateRow(SampleDbRow rowToAdd) {
		repo.insertOrUpdate(rowToAdd);
		
	}

}
