package pl.arceo.sstp.service;

import java.util.List;

import pl.arceo.sstp.dbModel.SampleDbRow;

public interface SampleService {

	List<SampleDbRow> selectSampleRows();

	void insertOrUpdateRow(SampleDbRow rowToAdd);

}
