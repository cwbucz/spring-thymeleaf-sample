package pl.arceo.sstp.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import pl.arceo.sstp.dbModel.SampleDbRow;

@Repository
public class SampleRepositoryImpl extends BaseRepositoryImpl implements SampleRepository {

	@Override
	public List<SampleDbRow> selectSampleRows() {
		return sessionFactory.getCurrentSession().createQuery("from SampleDbRow").list();
	}

}
