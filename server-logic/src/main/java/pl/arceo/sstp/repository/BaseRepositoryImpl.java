package pl.arceo.sstp.repository;

import org.hibernate.LockMode;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import pl.arceo.sstp.dbModel.BaseBean;


public class BaseRepositoryImpl implements BaseRepository {
	@Autowired
	protected SessionFactory sessionFactory;

	@Override
	public void insertOrUpdate(Object bean) {
		sessionFactory.getCurrentSession().saveOrUpdate(bean);
		
	}

	@Override
	public void delete(Object object) {
		sessionFactory.getCurrentSession().delete(object);
		
	}

	@Override
	public <T> T merge(T bean) {
		return (T) sessionFactory.getCurrentSession().merge(bean);
	}

	@Override
	public <T extends BaseBean> T load(Class<T> clazz, Long id) {
		return (T) sessionFactory.getCurrentSession().load(clazz, id);
	}

	

	@Override
	public <T extends BaseBean> T get(Class<T> clazz, Long id) {
		return (T) sessionFactory.getCurrentSession().get(clazz, id);
	}



	@Override
	public void lock(Object object) {
		LockOptions lockOptions = new LockOptions();
		lockOptions.setLockMode(LockMode.PESSIMISTIC_READ).setTimeOut(2*60);
		
		sessionFactory.getCurrentSession().buildLockRequest(lockOptions ).lock(object);
		
		
	}

	
	
	
}
