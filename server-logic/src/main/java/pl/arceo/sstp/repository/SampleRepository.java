package pl.arceo.sstp.repository;

import java.util.List;

import pl.arceo.sstp.dbModel.SampleDbRow;

public interface SampleRepository extends BaseRepository{

	List<SampleDbRow> selectSampleRows();

}
