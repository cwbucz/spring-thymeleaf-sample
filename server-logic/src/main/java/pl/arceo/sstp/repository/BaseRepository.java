package pl.arceo.sstp.repository;

import pl.arceo.sstp.dbModel.BaseBean;


public interface BaseRepository {
	
	
	void insertOrUpdate(Object bean);
	void delete(Object object);
	public <T> T merge(T bean);
	public <T extends BaseBean> T load(Class<T> clazz,Long id);
	public <T extends BaseBean> T get(Class<T> clazz,Long id);
	void lock(Object object);
}
