package pl.arceo.sstp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.arceo.sstp.dbModel.SampleDbRow;
import pl.arceo.sstp.service.SampleService;
import pl.arceo.sstp.web.model.IndexFormModel;

@Controller
public class IndexController {
	
	@Autowired private SampleService service;
	
	@ModelAttribute("formModel")
	private IndexFormModel prepareIndexFormModel(){
		return new IndexFormModel();
	}

	@RequestMapping(value="/",method=RequestMethod.GET)
	public String showIndex(Model model){
		List<SampleDbRow> rows = service.selectSampleRows();
		model.addAttribute("rows", rows);
		return "base";
	}
	@RequestMapping(value="/",method=RequestMethod.POST)
	public String handleFormPost(Model model,@ModelAttribute IndexFormModel formModel){
		SampleDbRow rowToAdd = new SampleDbRow();
		rowToAdd.setRowValue(formModel.getValue());
		service.insertOrUpdateRow(rowToAdd);
		return showIndex(model);
	}
	
}
